---
title: "New release: v1.3.0"
date: 2020-12-21T11:23:05+02:00
tags: ["release"]
draft: false
---

This version introduces new formating features:

* Table:
  * Width, style and alignment for each column of a table
  * Width, border and stripes of the table
* Document
  * Select where you want the TOC (Table Of Content) to be displayed in HTML files
  * Add revison information


<!--more-->
### Release note

#### New features:
* Handle column formating in tables --> [#34](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/34)
* Handle table attributes --> [#33](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/33)
* Handle TOC position for HTML5 output --> [#29](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/29)
* Handle document revision number, date and remark --> [#32](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/32)
* Add accessor to get sections contain in a subsection --> [#30](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/30)

### Package Download

Package is directly available through the free version of [VI Package Manager](https://vipm.jki.net/get).

### LabVIEW supported version

2014 (32 and 64 bit) and above
