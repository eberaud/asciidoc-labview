---
title: "New release: v1.5.0"
date: 2022-06-08T11:43:05+02:00
tags: ["release"]
draft: false
---

This version introduces the capability to render your adoc file into PDF or HTLM5 files directly in your LabVIEW applications.

With this capability, it's never been easier to get rid of MS Office and other complex solutions to generate beautiful documents and reports in LabVIEW.

### Asciidoctor toolchain install during VIP installation

During the package installation, the Ascidoctor toolchain is installed locally with a minimum footprint.
Rendering feature supported by the installation:
* Rendering adoc files to HTML5 and PDF
* Diagram generation (web connection required)
* STEM expressions rendered (HTML5 output only)

### 3 new functions available in the palette

* **Render Adoc File:** Takes an adoc file as input and generates HTML5 or PDF output next to the adoc file
* **Check Asciidoctor Toolchain Installation:** Verify if the toolchain is correctly installed
* **Installing Asciidoctor Toolchain:** Process to the toolchain installation. This function is useful when you want to ingrate rendering to your application. 

### Contributor

Big thanks to [Matthias Baudot](https://www.linkedin.com/in/matthias-baudot-4b9b0537/) for his help with the installation process.

<!--more-->
### Release note

#### New features:
* Provide a function to silently install the asciidoctor toolchain locally --> [#39](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/39)
* Provide a function to render the final document from an adoc file --> [#40](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/40)

### Package Download

The package is directly available through the free version of [VI Package Manager](https://vipm.jki.net/get).

### LabVIEW supported version

2014 (32 and 64 bit) and above